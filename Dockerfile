FROM node:11.10.1-alpine

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

# RUN yarn build
# RUN yarn docs:build

EXPOSE 3000
CMD [ "node", "server.js" ]