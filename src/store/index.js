import Vue from 'vue'
import Vuex from 'vuex'
import * as mutations from './mutation-types'
import { breeds as breedList } from '@/components/common/data/breeds.json'

const validSubs = require('@/components/combination-calculator/data/validSubs.json')

Vue.use(Vuex)

const combinationModule = {
  state: {
    breedList,
    breeds: {
      parent1Main: 'ape',
      parent1Sub: 'ape',
      parent2Main: 'ape',
      parent2Sub: 'ape',
      childMain: 'beaclon',
      childSub: 'beaclon'
    },
    desiredOrder: [
      'lif',
      'pow',
      'int',
      'ski',
      'spd',
      'def'
    ],
    hardMode: false
  },
  getters: {
    child: state => ({
      main: state.breeds.childMain,
      sub: state.breeds.childSub
    }),
    parent1: state => ({
      main: state.breeds.parent1Main,
      sub: state.breeds.parent1Sub
    }),
    parent2: state => ({
      main: state.breeds.parent2Main,
      sub: state.breeds.parent2Sub
    }),
  },
  mutations: {
    [mutations.SET_BREED](state, payload) {
      const { breedToUpdate, breedName } = payload
      state.breeds[breedToUpdate] = breedName

      // If setting the main breed invalidates the sub breed, set the new sub equal to the new main
      if (breedToUpdate.endsWith('Main')) {
        const subBreed = this.getters[breedToUpdate.replace('Main', '')].sub
        if (!validSubs[breedName].includes(subBreed)) {
          state.breeds[breedToUpdate.replace('Main', 'Sub')] = breedName
        }
      }
    },
    [mutations.SET_BREED_LIST](state, payload) {
      state.breedList = payload
    },
    [mutations.SET_CHILD_ORDER](state, payload) {
      state.desiredOrder = payload
    },
    [mutations.SET_HARD_MODE](state) {
      state.hardMode = !state.hardMode
    }
  }
}

export default new Vuex.Store({
  modules: {
    combination: combinationModule,
  }
})