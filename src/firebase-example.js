/* If you want to deploy this site yourself using your own firebase database you'll need to follow the firebase tutorial
 * and replace the object below accordingly, then rename this file to 'firebase.js'.
 *
 * The real firebase.js should be excluded from verion control as it has sensitive information.
 */

import firebase from 'firebase/app'

export default firebase
  .initializeApp({
    apiKey: 'key',
    authDomain: 'my-app.firebaseapp.com',
    databaseURL: 'https://my-app.firebaseio.com',
    projectId: 'my-app',
    storageBucket: 'my-app.appspot.com',
    messagingSenderId: '123456789012'
  })
  .database()