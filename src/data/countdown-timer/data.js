const hexT = "20,30 30,20 80,20 90,30 80,40 30,40|"
const hexTL = "18,32 28,42 28,92 18,102 8,92 8,42|"
const hexTR = "92,32 102,42 102,92 92,102 82,92 82,42|"
const hexM = "20,104 30,94 80,94 90,104 80,114 30,114|"
const hexBL = "18,106 28,116 28,166 18,176 8,166 8,116|"
const hexBR = "92,106 102,116 102,166 92,176 82,166 82,116|"
const hexB = "20,178 30,168 80,168 90,178 80,188 30,188|"

const ZERO = hexT +
  hexTL +
  hexTR +
  hexBL + // "55,174 55,174 55,174 55,174 55,174 55,174|" +
  hexBL +
  hexBR +
  hexB

const ONE = hexBR + //"55,100 55,100 55,100 55,100 55,100 55,100|" +
  hexTR +
  hexTR +
  hexTR + // "55,174 55,174 55,174 55,174 55,174 55,174|" +
  hexBR +
  hexBR +
  hexTR // "55,248 55,248 55,248 55,248 55,248 55,248|"

const TWO = hexT +
  hexTR +
  hexTR +
  hexM +
  hexBL +
  hexBL +
  hexB

const THREE = hexT +
  hexTR +
  hexTR +
  hexM +
  hexBR +
  hexBR +
  hexB

const FOUR = hexTR +
  hexTL +
  hexTR +
  hexM +
  hexTL +
  hexBR +
  hexM

const FIVE = hexT +
  hexTL +
  hexTL +
  hexM +
  hexTL +
  hexBR +
  hexB

const SIX = hexT +
  hexTL +
  hexTL +
  hexM +
  hexBL +
  hexBR +
  hexB

const SEVEN = hexT +
  hexTR +
  hexTR +
  hexTR +
  hexBR +
  hexBR +
  hexBR

const EIGHT = hexT +
  hexTL +
  hexTR +
  hexM +
  hexBL +
  hexBR +
  hexB

const NINE = hexT +
  hexTL +
  hexTR +
  hexM +
  hexTR + //"18,216 18,216 18,216 18,216 18,216 18,216|" +
  hexBR +
  hexB

export function mapToDigit(i, randomMode) {

  if (!randomMode) {
    switch (i) {
      case 0:
        return ZERO
      case 1:
        return ONE
      case 2:
        return TWO
      case 3:
        return THREE
      case 4:
        return FOUR
      case 5:
        return FIVE
      case 6:
        return SIX
      case 7:
        return SEVEN
      case 8:
        return EIGHT
      case 9:
        return NINE
    }
  }
  else {
    let validHexes = []
    switch (i) {
      case 0:
        validHexes.push(hexT, hexTL, hexTR, hexBL, hexBR, hexB)
        break;
      case 1:
        validHexes.push(hexTR, hexBR)
        break;
      case 2:
        validHexes.push(hexT, hexTR, hexM, hexBL, hexB)
        break;
      case 3:
        validHexes.push(hexT, hexTR, hexM, hexBR, hexB)
        break;
      case 4:
        validHexes.push(hexTL, hexTR, hexM, hexBR)
        break;
      case 5:
        validHexes.push(hexT, hexTL, hexM, hexBR, hexB)
        break;
      case 6:
        validHexes.push(hexT, hexTL, hexM, hexBL, hexBR, hexB)
        break;
      case 7:
        validHexes.push(hexT, hexTR, hexBR)
        break;
      case 8:
        validHexes.push(hexT, hexTL, hexTR, hexM, hexBL, hexBR, hexB)
        break;
      case 9:
        validHexes.push(hexT, hexTL, hexTR, hexM, hexBR, hexB)
        break;
    }
    let digit = ''
    let nextHex = ''
    let validHexesOriginal = validHexes.slice()

    for (i = 0; i < 7; i++) {
      if (validHexes.length > 0) {
        nextHex = validHexes.splice(Math.floor(Math.random() * validHexes.length), 1)
        digit += nextHex[0]
      }
      else {
        digit += validHexesOriginal[Math.floor(Math.random() * validHexesOriginal.length)];
      }
    }
    return digit
  }
  return ZERO
}
