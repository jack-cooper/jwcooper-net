import App from './App'
import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'
import Inkline from '@inkline/inkline'
import InklineValidation from '@inkline/inkline/dist/validation'
import router from './router'
import store from './store'
import Vue from 'vue'
import VueApexCharts from 'vue-apexcharts'
import './firebase'
import './global-filters'
import '@inkline/inkline/dist/inkline.css'
import './assets/styles/index.css'

Vue.config.productionTip = false
Vue.use(Inkline)
Vue.use(InklineValidation)
Vue.use(VueApexCharts)

Vue.component('ApexChart', VueApexCharts)

firebase.auth().onAuthStateChanged(() => {
  let app
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
