import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase/app'
import CombinationCalculator from './views/CombinationCalculator'
import CountdownTimerView from './views/CountdownTimerView'
import Home from './views/Home'
import Login from './views/Login'
import LazyLatinSquares from './views/LazyLatinSquares'
import NotFoundView from './views/NotFoundView'
import SignUp from './views/SignUp'
import TechTables from './views/TechTables'
import Tourneys from './views/tourneys/Tourneys'
import TourneyCreate from './views/tourneys/TourneyCreate'
import TourneyOverview from './views/tourneys/TourneyOverview'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: 'home'
    },
    {
      path: '/combination-calculator',
      name: 'combinationCalculator',
      component: CombinationCalculator
    },
    {
      path: '/countdown-timer',
      name: 'countdownTimer',
      component: CountdownTimerView
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/latin-squares',
      name: 'latinSquares',
      component: LazyLatinSquares
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signUp',
      component: SignUp
    },
    {
      path: '/tech-tables',
      name: 'techTables',
      component: TechTables
    },
    {
      path: '/tourneys',
      name: 'tourneys',
      component: Tourneys,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/tourneys/create',
      name: 'tourneyCreate',
      component: TourneyCreate,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/tourneys/id/:tid',
      name: 'tourneyOverview',
      component: TourneyOverview,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: NotFoundView
    }
  ]
})

router.beforeEach((to, _from, next) => {
  const currentUser = firebase.auth().currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  // Redirect login and signup to home if user is already logged in
  if (
    !requiresAuth &&
    currentUser &&
    (to.name === 'login' || to.name === 'signUp')
  ) {
    next('home')
    return
  }

  // Send to login if the page requires it and user is not logged in
  if (requiresAuth && !currentUser) {
    next({ name: 'login' })
  } else {
    next()
  }
})

export default router
