import Vue from 'vue'

Vue.filter('capitalize', value => {
  if (!value) return ''
  return value.replace(/\b([a-z])/g, char => char.toUpperCase()).replace(/Arrowhead/g, 'Arrow Head').replace(/Colorpandora/g, 'ColorPandora')
})