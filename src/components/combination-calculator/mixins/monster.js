const zeroStats = {
  lif: 0,
  pow: 0,
  int: 0,
  skl: 0,
  spd: 0,
  def: 0
}

export const monster = {
  computed: {
    baseStats() {
      const { breedString, hardMode } = this
      const baseStatsList = hardMode
        ? require('../data/baseStatsHard.json')
        : require('../data/baseStats.json')
      return baseStatsList[breedString] || zeroStats
    },
    breedString() {
      const { breeds } = this
      return breeds.main + '/' + breeds.sub
    },
    correctedStats() {
      const { correctStats, stats } = this
      return correctStats(stats)
    },
    gains() {
      const { breeds, breedString, hardMode } = this
      let gains

      // Hard Mode fixes the breeds with bugged gains
      if (hardMode) {
        gains = require('../data/gainsHard.json')
      }
      // Directly return gains for bugged breeds, else carry on as normal
      else {
        const gainsBugged = require('../data/gainsBugged.json')
        if (gainsBugged[breedString]) {
          return gainsBugged[breedString]
        }
        gains = require('../data/gains.json')
      }

      const mainGains = gains[breeds.main]
      const subGains = gains[breeds.sub]

      let avgGains = {}
      Object.keys(mainGains).forEach(
        key =>
          (avgGains[key] = Math.round(
            mainGains[key] * 0.6 + subGains[key] * 0.4
          ))
      )
      return avgGains
    },
    hardMode() {
      return this.$store.state.combination.hardMode
    }
  },
  methods: {
    correctStats(stats) {
      const { gains } = this

      let corrected = {}
      Object.keys(stats).forEach(key => {
        corrected[key] = stats[key] * (0.5 * (gains[key] - 1))
      })
      return corrected
    },
    getStatOrder(tiebreaker) {
      const { baseStats } = this
      const priority = ['lif', 'pow', 'int', 'ski', 'spd', 'def']

      return Object.fromEntries(
        Object.entries(tiebreaker).sort((stat1, stat2) => {
          const [s1Label, s1Value] = stat1
          const [s2Label, s2Value] = stat2

          return (
            s2Value - s1Value ||
            baseStats[s2Label] - baseStats[s1Label] ||
            priority.indexOf(s2Label) - priority.indexOf(s1Label)
          )
        })
      )
    }
  }
}
