# Errantry

Errantry is where your monster goes to learn new [techniques](/techniques/). Assuming your monster is not injured, it will take 4 weeks in total, regardless of failures or whether your monster fights an enemy at the end. There are 5 errantry locations, with the first 4 available to any monster, while the Kawrea errantry is only available to monsters which are B rank or higher. Each errantry course corresponds to a specific type of technique that your monster can learn. Each of the errantry courses will raise `Lif` a small amount, and one of the 5 remaining stats a large amount. For exact numbers see [here](/raising/stat-gains.md).

## Courses

| Course Name | Stat Increased | Technique Type |
|-------------|----------------|----------------|
| Papas       | Spd            | Sharp          |
| Mandy       | Pow            | Heavy          |
| Parepare    | Int            | Withering      |
| Torble      | Ski            | Hit            |
| Kawrea      | Def            | Special        |