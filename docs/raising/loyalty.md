# Loyalty

Your monster's loyalty is actually the average (rounded down) of 2 internal values, both confined to a 0-100 scale. These values are **spoil** and **fear**. A perfectly loyal monster must therefore have 100 spoil _and_ 100 fear. In general, fear is much harder to increase than spoil.

**30 Spoil, 10 Fear**  
`Loyalty = (30 + 10) / 2 = 20`

**50 Spoil, 100 Fear**  
`Loyalty = (50 +100) / 2 = 75`

**100 Spoil, 99 Fear**  
`Loaylty = (100 + 99) / 2 = 99.5 (always round down) = 99`