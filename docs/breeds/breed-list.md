---
sidebarDepth: 0
---

# List of All Monsters

<img src="../.vuepress/images/hero.png" />

## Pixie

1. **Pixie** | Pixie/Pixie  
   **Card Rarity**: E  
   There are many fans though it is spoiled and powerless.

2. **Daina** | Pixie/Dragon  
   **Card Rarity**: D  
   Because it is of legendary blood, it has high pride.

3. **Unico** | Pixie/Centaur  
   **Card Rarity**: C  
   It is the most earnest monster in the Pixie species.

4. **Jilt** | Pixie/Wracky  
   **Card Rarity**: C  
   It is known that Jilt likes to tease men.

5. **Granity** | Pixie/Golem  
   **Card Rarity**: D  
   It has a cold atmosphere maybe because of its strong will.

6. **Dixie** | Pixie/Zuum  
   **Card Rarity**: E  
   It likes wild life because it was born in a jungle.

7. **Janne** | Pixie/Durahan  
   **Card Rarity**: C  
   It is brave but its heart is a little bit too delicate to defeat enemies.

8. **Mint** | Pixie/Tiger  
   **Card Rarity**: E  
   It has charming bluish hair and a cute tail.

9. **Lepus** | Pixie/Hare  
   **Card Rarity**: E  
   In winter, warm air is held in its fur and keeps it warm.

10. **Angel** | Pixie/Gali  
    **Card Rarity**: D  
    They say Angel is the most merciful monster of all.

11. **Kitten** | Pixie/Kato  
    **Card Rarity**: E  
    A musical, based on Kitten's life has played a long run.

12. **Jinnee** | Pixie/Bajarl  
    **Card Rarity**: C  
    It is sloppy by nature and sleeps often.

13. **Futurity** | Pixie/Metalner  
    **Card Rarity**: B  
    Its looks may be what humans look like in the future.

14. **Vanity** | Pixie/Suezo  
    **Card Rarity**: E  
    This name was given by Dr. Maccots when he found it.

15. **Snowy** | Pixie/Jill  
    **Card Rarity**: C  
    It is mentioned in the legends of some northern countries.

16. **Lilim** | Pixie/Joker  
    **Card Rarity**: B  
    It can be best described as a little witch.

17. **Nagisa** | Pixie/Jell  
    **Card Rarity**: E  
    "Nagisa Skin." This admiring word is derived from this one.

18. **Dryad** | Pixie/Mock  
    **Card Rarity**: C  
    This monster is a tree fairy. It inhabits in the forest.

19. **Serenity** | Pixie/Plant  
    **Card Rarity**: D  
    In spite of its photosynthesis ability, it loves to eat.

20. **Silhouette** | Pixie/Monol  
    **Card Rarity**: D  
    It has a shield wrapped around it in the FIMBA area.

21. **Night Flyer** | Pixie/Worm  
    **Card Rarity**: C  
    Some people consider its beautiful feathers grotesque.

22. **Allure** | Pixie/Naga  
    **Card Rarity**: D  
    They say that it sucks the life force out of men.

23. **Poison** | Pixie/???  
    **Card Rarity**: S  
    They say it is hysterical. But the truth is unknown.

24. **Kasumi** | Pixie/???  
    **Card Rarity**: A  
    Its breasts tend to get more attention than its battles.

25. **Mia** | Pixie/???  
    **Card Rarity**: S  
    It draws many people's support with its cute charm.

## Dragon

26. **Tiamat** | Dragon/Pixie  
    **Card Rarity**: D  
    Everybody agrees on its strength, but few agrees on its looks.

27. **Dragon** | Dragon/Dragon  
    **Card Rarity**: D  
    Everybody knows this legendary monster. But few can raise it.

28. **Corkasus** | Dragon/Beaclon  
    **Card Rarity**: C  
    It is difficult to find it since it hardly leaves its homeland.

29. **Tecno Dragon** | Dragon/Henger  
    **Card Rarity**: C  
    They say it was made with all the ancient technologies.

30. **Stone Dragon** | Dragon/Golem  
    **Card Rarity**: C  
    It is afraid its destructive power may affect others, and hides in a cave.

31. **Armor Dragon** | Dragon/Durahan  
    **Card Rarity**: B  
    They say it was used by humans for the ancient battles.

32. **Crab Dragon** | Dragon/Arrow Head  
    **Card Rarity**: D  
    It inhabits the lake and rules that lake.

33. **Hound Dragon** | Dragon/Tiger  
    **Card Rarity**: D  
    It has a habit of hiding things like a dog.

34. **Gariel** | Dragon/Gali  
    **Card Rarity**: C  
    Its head is like holography, for it has a kind of deity.

35. **Oscerot** | Dragon/Kato  
    **Card Rarity**: D  
    This monster keeps a collection of oil pot in his house.

36. **Dodongo** | Dragon/Bajarl  
    **Card Rarity**: C  
    This dragon can only be seen in the desert area.

37. **Gidras** | Dragon/Metalner  
    **Card Rarity**: B  
    This dragon has the mysterious power of Metalner.

38. **Death Dragon** | Dragon/Joker  
    **Card Rarity**: B  
    It is said that this dragon was resurrected by Joker's power.

39. **Ragnaroks** | Dragon/Monol  
    **Card Rarity**: D  
    They say the ancient culture might have been destroyed by it.

40. **Moo** | Dragon/???  
    **Card Rarity**: S  
    This dreadful looking dragon hardly appears even in legends.

## Centaur

41. **Ferious** | Centaur/Pixie  
    **Card Rarity**: C  
    It has the upper body of a Pixie and the lower body of a horse.

42. **Dragoon** | Centaur/Dragon  
    **Card Rarity**: C  
    This monster is the mixture of a brave Dragon and a just Centaur.

43. **Centaur** | Centaur/Centaur  
    **Card Rarity**: D  
    This monster is intelligent, just and proud.

44. **Trojan** | Centaur/Golem  
    **Card Rarity**: C  
    It has stonelike hard skin and a stubborn character.

45. **Chariot** | Centaur/Durahan  
    **Card Rarity**: B  
    It is said that it holds the souls of Durahan and Centaur.

46. **Antares** | Centaur/Arrow Head  
    **Card Rarity**: D  
    It is expected to function well both in water and on land.

47. **Celious** | Centaur/Tiger  
    **Card Rarity**: D  
    It is a loner and does not like to cluster.

48. **Bazoo** | Centaur/Bajarl  
    **Card Rarity**: B  
    It resides in a harsh location, and has its own values.

49. **Reaper** | Centaur/Joker  
    **Card Rarity**: B  
    It was created by Joker, but it even hates him.

50. **Trotter** | Centaur/???  
    **Card Rarity**: A  
    It has an excellent reputation as an excellent runner.

51. **Blue Thunder** | Centaur/???  
    **Card Rarity**: A  
    This tribe is considered to be the descendant of Thor.

## ColorPandora

52. **PeachTreeBug** | ColorPandora/Pixie  
    **Card Rarity**: D  
    It is cute, but you should know that it is a bit too noisy.

53. **ColorPandora** | ColorPandora/ColorPandora  
    **Card Rarity**: D  
    This monster has three wills. Victory, love, and effort.

54. **Liquid Cube** | ColorPandora/Jell  
    **Card Rarity**: D  
    It is almost impossible to recognize it in the water.

55. **Dice** | ColorPandora/???  
    **Card Rarity**: A  
    This monster has strange patterns on its body.

56. **Tram** | ColorPandora/???  
    **Card Rarity**: A  
    This monster is covered by a house-like pattern.

## Beaclon

57. **Bethelgeus** | Beaclon/Dragon  
    **Card Rarity**: C  
    The image of its flying in the sky is called "Death's Dance."

58. **Beaclon** | Beaclon/Beaclon  
    **Card Rarity**: C  
    It has a strong body and much power. However, its IQ is low.

59. **Melcarba** | Beaclon/Henger  
    **Card Rarity**: C  
    A mechanical monster who was made before Henger.

60. **Rocklon** | Beaclon/Golem  
    **Card Rarity**: C  
    Its rocky body may be hollow inside because it can fly.

61. **Centurion** | Beaclon/Durahan  
    **Card Rarity**: B  
    Its body is covered with well-designed solid armor.

62. **Sloth Beetle** | Beaclon/Tiger  
    **Card Rarity**: C  
    It resides in a high mountain region, that is covered in snow.

63. **KautRoarKaut** | Beaclon/Bajarl  
    **Card Rarity**: B  
    It is used as a means of transportation by some people.

64. **Jaggernaut** | Beaclon/Joker  
    **Card Rarity**: B  
    Its carmine eyes show its wickedness.

65. **Ducklon** | Beaclon/Ducken  
    **Card Rarity**: C  
    It has a spring that has to be wound each morning.

66. **Eggplantern** | Beaclon/???  
    **Card Rarity**: A  
    It is said that those who waste food are scolded by this monster.

## Henger

67. **Garlant** | Henger/Dragon  
    **Card Rarity**: C  
    It is considered to be a phantom monster in the FIMBA area.

68. **Henger** | Henger/Henger  
    **Card Rarity**: D  
    Monster's souls in Henger rarely maintain their past memories.

69. **Gaia** | Henger/Golem  
    **Card Rarity**: D  
    It is a mechanical monster with a soul from the Golem species.

70. **Omega** | Henger/Zuum  
    **Card Rarity**: D  
    It is one of the reason why Zuum and Dino are similar.

71. **Heuy** | Henger/Metalner  
    **Card Rarity**: C  
    You will be surprised to see it flying around in the air fast.

72. **End Bringer** | Henger/Joker  
    **Card Rarity**: B  
    This mechanical monster's soul has been infused by Joker.

73. **Automaton** | Henger/Mock  
    **Card Rarity**: B  
    It is said that it was made as a prototype for Henger.

74. **Black Henger** | Henger/Monol  
    **Card Rarity**: D  
    Its black body is an effective camouflage in the night.

75. **Skeleton** | Henger/???  
    **Card Rarity**: A  
    Its design is a reminder that death is always with us.

## Wracky

76. **Baby Doll** | Wracky/Pixie  
    **Card Rarity**: C  
    This monster is a doll of Pixie with a wicked soul.

77. **Draco Doll** | Wracky/Dragon  
    **Card Rarity**: C  
    The original version of this doll is sold at premium price.

78. **Henger Doll** | Wracky/Henger  
    **Card Rarity**: C  
    It is a doll of Henger with a wicked soul.

79. **Wracky** | Wracky/Wracky  
    **Card Rarity**: C  
    It carries the grudges of the monsters killed in the battle.

80. **Pebbly** | Wracky/Golem  
    **Card Rarity**: C  
    This Golem doll is made of stone and is too heavy for children.

81. **Petit Knight** | Wracky/Durahan  
    **Card Rarity**: B  
    It is the most popular doll and is often out of stock.

82. **Bakky** | Wracky/Bajarl  
    **Card Rarity**: C  
    It never used to be bought at the shop, but it has become popular.

83. **Metal Glay** | Wracky/Metalner  
    **Card Rarity**: A  
    This is popular among little kids and tends to be their toy.

84. **Tricker** | Wracky/Joker  
    **Card Rarity**: A  
    Contrary to its humorous appearance, its nature is rather evil.

85. **Mocky** | Wracky/Mock  
    **Card Rarity**: B  
    Mention of its cheap-looking makes this monster mad.

86. **Santa Clause** | Wracky/???  
    **Card Rarity**: S  
    A festival doll became possessed and then became this monster.

## Golem

87. **Pink Golem** | Golem/Pixie  
    **Card Rarity**: D  
    It is a smart and quiet monster. It doesn't like battle.

88. **Tyrant** | Golem/Dragon  
    **Card Rarity**: C  
    The wings on its back are to small for it to fly.

89. **Strong Horn** | Golem/Beaclon  
    **Card Rarity**: C  
    Though it has a large horn, it hardly uses it in battle.

90. **Gobi** | Golem/Henger  
    **Card Rarity**: C  
    It is said that this monster was a weapon for the ancient war.

91. **Mariomax** | Golem/Wracky  
    **Card Rarity**: C  
    It always cares how it looks. It is a little bit self conceited.

92. **Golem** | Golem/Golem  
    **Card Rarity**: D  
    It moves slowly, but its punch is destructive.

93. **Scaled Golem** | Golem/Zuum  
    **Card Rarity**: D  
    Some ornaments are made out of its green scale.

94. **Battle Rocks** | Golem/Durahan  
    **Card Rarity**: B  
    It is said that a giant's armor turned into this monster.

95. **Dagon** | Golem/Arrow Head  
    **Card Rarity**: D  
    Its hard body can even resist the pressures in the deep ocean.

96. **Big Blue** | Golem/Tiger  
    **Card Rarity**: D  
    Its body is made of ice rock, which will not ever melt.

97. **Moaigon** | Golem/Hare  
    **Card Rarity**: D  
    Its blue eyes remember the sad history between humans and them.

98. **Sleepyhead** | Golem/Baku  
    **Card Rarity**: D  
    It spends most of its time in one place and hardly moves.

99. **Amenhotep** | Golem/Gali  
    **Card Rarity**: C  
    It is popular among noblemen because of its beautiful body.

100.  **Pressure** | Golem/Zilla  
      **Card Rarity**: B  
      It cannot float on the water like Zilla. It is too heavy.

101.  **Dao** | Golem/Bajarl  
      **Card Rarity**: C  
      It moves each part of its body by using a mysterious magic.

102.  **Astro** | Golem/Metalner  
      **Card Rarity**: B  
      It is said that this monster contains a Metalner in it.

103.  **Titan** | Golem/Suezo  
      **Card Rarity**: D  
      It loves curry rice, but that is not why its body is yellow.

104.  **Angolmor** | Golem/Joker  
      **Card Rarity**: B  
      It is a Golem under control of Joker's soul.

105.  **Poseidon** | Golem/Jell  
      **Card Rarity**: D  
      Though it wishes to swim the crawl, it is afraid of water.

106.  **Wood Golem** | Golem/Mock  
      **Card Rarity**: B  
      Unlike Mock, it rarely changes the expression on its face.

107.  **Ecologuardia** | Golem/Plant  
      **Card Rarity**: D  
      It is worshipped as God of nature at small shrines in some cities.

108.  **Black Golem** | Golem/Monol  
      **Card Rarity**: D  
      People cannot do bad things when they think of it watching them.

109.  **Magna** | Golem/Worm  
      **Card Rarity**: D  
      The patterns on its body is considered to be ancient characters.

110.  **Marble Guy** | Golem/Naga  
      **Card Rarity**: D  
      Unlike other Golem monsters, it is rather offensive.

111.  **ForwardGolem** | Golem/???  
      **Card Rarity**: A  
      This monster has a strong will to rush into its goals.

## Zuum

112. **FairySaurian** | Zuum/Pixie  
     **Card Rarity**: E  
     We can ride on its back because it has no wings on its back.

113. **Salamander** | Zuum/Dragon  
     **Card Rarity**: C  
     Its IQ is lower than Dragon's. It is very fast and violent.

114. **Rock Saurian** | Zuum/Golem  
     **Card Rarity**: D  
     It can move quicker than its looks, but it is not very smart.

115. **Zuum** | Zuum/Zuum  
     **Card Rarity**: E  
     It's pedigree is considered to be similar to the Dino species.

116. **Crab Saurian** | Zuum/Arrow Head  
     **Card Rarity**: E  
     It can swim fast in water and it is also a fast runner.

117. **HoundSaurian** | Zuum/Tiger  
     **Card Rarity**: E  
     It can run fast, so it is often used to deliver express mail.

118. **Spot Saurian** | Zuum/Hare  
     **Card Rarity**: E  
     Its character is quiet, but it is brave when protecting animals.

119. **Hachiro** | Zuum/Baku  
     **Card Rarity**: D  
     It is more intelligent than people think it is.

120. **NobleSaurian** | Zuum/Gali  
     **Card Rarity**: D  
     It is considered to be a status symbol among noble people.

121. **Tasman** | Zuum/Kato  
     **Card Rarity**: E  
     This monster believes only in brute strength.

122. **Sand Saurian** | Zuum/Bajarl  
     **Card Rarity**: C  
     Most monsters dislike the desert, but this one does not.

123. **Mustardy** | Zuum/Suezo  
     **Card Rarity**: E  
     It is playful, and tends to carry its jokes too far.

124. **Basilisk** | Zuum/Joker  
     **Card Rarity**: B  
     It confuses its enemies with a special pattern on its body.

125. **JellySaurian** | Zuum/Jell  
     **Card Rarity**: E  
     Its body is made up gel and can be a comfortable type of cushion.

126. **Wood Saurian** | Zuum/Mock  
     **Card Rarity**: B  
     It is cowardly and runs into the forest when it sees humans.

127. **AlohaSaurian** | Zuum/Plant  
     **Card Rarity**: E  
     Girls dream of riding on this one when they get married.

128. **BlackSaurian** | Zuum/Monol  
     **Card Rarity**: E  
     Its body with a black luster attracts fashionable people.

129. **ShellSaurian** | Zuum/Worm  
     **Card Rarity**: D  
     Various Knights rode on its back and fight in the ancient era.

130. **Naga Saurian** | Zuum/Naga  
     **Card Rarity**: E  
     Since it is a descendant from Naga, it is rather violent.

131. **ZebraSaurian** | Zuum/???  
     **Card Rarity**: B  
     Its striped body is the least conspicuous in savanna region.

## Durahan

132. **Lesziena** | Durahan/Pixie  
     **Card Rarity**: C  
     It proves that women also participated in ancient battles.

133. **Vesuvius** | Durahan/Dragon  
     **Card Rarity**: B  
     Its helmet has a curved dragon figure on it.

134. **Hercules** | Durahan/Beaclon  
     **Card Rarity**: B  
     It has excellent strength and a dull sword won't even scratch it.

135. **Kelmadics** | Durahan/Golem  
     **Card Rarity**: C  
     This stone armor is too heavy for normal people to wear.

136. **Durahan** | Durahan/Durahan  
     **Card Rarity**: C  
     Ancient armor has mutated into the monster with this name.

137. **Lorica** | Durahan/Arrow Head  
     **Card Rarity**: C  
     This armor, made out of the shell of Arrow Heads, is light and easy to handle.

138. **Hound Knight** | Durahan/Tiger  
     **Card Rarity**: C  
     Its helmet is shaped like a Tiger's figure.

139. **Garuda** | Durahan/Phoenix  
     **Card Rarity**: A  
     It is said that the legendary general, Garuda, used to wear it.

140. **Metal Glory** | Durahan/Metalner  
     **Card Rarity**: A  
     There is no record showing that this armor has been used in the past.

141. **Genocider** | Durahan/Joker  
     **Card Rarity**: B  
     It is said that a curse has been put upon this armor.

142. **Wood Knight** | Durahan/Mock  
     **Card Rarity**: B  
     Poor knights made this armor out of trees in the ancient era.

143. **Shogun** | Durahan/???  
     **Card Rarity**: A  
     This armor was discovered in an eastern country.

144. **Ruby Knight** | Durahan/???  
     **Card Rarity**: A  
     It is said that it was the armor for the ancient royal guard.

145. **Kokushi Muso** | Durahan/???  
     **Card Rarity**: A  
     This name means that no one can equal it.

## Arrow Head

146. **Renocraft** | Arrow Head/Henger  
     **Card Rarity**: D  
     This monster is a prototype made by engineers prior to Hengers.

147. **Priarocks** | Arrow Head/Golem  
     **Card Rarity**: D  
     This monster inhabits the desert and it is nocturnal.

148. **Plated Arrow** | Arrow Head/Durahan  
     **Card Rarity**: C  
     The real design of this monster is in the form of vapor.

149. **Arrow Head** | Arrow Head/Arrow Head  
     **Card Rarity**: E  
     Since it has a very hard shell, it is hardly KO'd in battle.

150. **MustardArrow** | Arrow Head/Suezo  
     **Card Rarity**: E  
     Dropping tears does not always mean sadness.

151. **Selketo** | Arrow Head/Joker  
     **Card Rarity**: C  
     It has thousands of thorns and stings whoever touches it.

152. **Log Sawer** | Arrow Head/Mock  
     **Card Rarity**: C  
     Its body is made out of solid oak and is not soft but imflammable

153. **Sumopion** | Arrow Head/???  
     **Card Rarity**: B  
     It is strange looking, but it does practice hard every day.

## Tiger

154. **Daton** | Tiger/Pixie  
     **Card Rarity**: E  
     It is quite playful, but sometimes it injures its trainers.

155. **Rock Hound** | Tiger/Golem  
     **Card Rarity**: D  
     It cannot run fast because its body is made out of rocks.

156. **Datonare** | Tiger/Zuum  
     **Card Rarity**: E  
     No one equals its speed when it runs through the forest.

157. **Tiger** | Tiger/Tiger  
     **Card Rarity**: E  
     Many ordinary people keep this monster.

158. **Hare Hound** | Tiger/Hare  
     **Card Rarity**: E  
     This monster is very cute and will make you want to keep it.

159. **Balon** | Tiger/Gali  
     **Card Rarity**: D  
     Because of its solemn nature, ordinary people doesn't keep it.

160. **Mono Eyed** | Tiger/Suezo  
     **Card Rarity**: E  
     It is weak in measuring exact distance with its single eye.

161. **Jelly Hound** | Tiger/Jell  
     **Card Rarity**: E  
     After it passes by, even warm air turns into cool air.

162. **Tropical Dog** | Tiger/Plant  
     **Card Rarity**: E  
     It is popular mainly in the southern region.

163. **Terror Dog** | Tiger/Monol  
     **Card Rarity**: E  
     It takes extra care properly maintain its beautiful hair.

164. **Jagd Hound** | Tiger/Worm  
     **Card Rarity**: D  
     Its four eyes will not function at the same time.

165. **Cabalos** | Tiger/Naga  
     **Card Rarity**: E  
     Ordinary people are prohibited from keeping this monster.

166. **White Hound** | Tiger/???  
     **Card Rarity**: B  
     This monster has pure white hair and much is made of it.

## Hopper

167. **Fairy Hopper** | Hopper/Pixie  
     **Card Rarity**: D  
     It can jump a little higher that other hoppers.

168. **Draco Hopper** | Hopper/Dragon  
     **Card Rarity**: C  
     Though its potential is high, few people raise suck a scary looking monster.

169. **Skipper** | Hopper/Tiger  
     **Card Rarity**: E  
     It will move seductively when someone touches its horn.

170. **Hopper** | Hopper/Hopper  
     **Card Rarity**: E  
     It is a naughty monster, but it is loved by many trainers.

171. **Mustachios** | Hopper/Kato  
     **Card Rarity**: E  
     This monster is always clustered with companions.

172. **Emerald Eye** | Hopper/Bajarl  
     **Card Rarity**: C  
     It is said that it loves to eat jewelry, especially emeralds.

173. **Springer** | Hopper/Metalner  
     **Card Rarity**: B  
     Its body shows a soft luster because of its Metalner blood.

174. **Rear Eyed** | Hopper/Suezo  
     **Card Rarity**: E  
     It has the third eye in the back of its head.

175. **Snow Hopper** | Hopper/Jill  
     **Card Rarity**: B  
     Snowboarding was created by copying the way this monster skis.

176. **Pink Hopper** | Hopper/Mocchi  
     **Card Rarity**: E  
     It is a combined monster from two other popular monsters, but it is not
     popular.

177. **Sneak Hopper** | Hopper/Joker  
     **Card Rarity**: B  
     It is very self centered and it has a cunning character.

178. **Woody Hopper** | Hopper/Mock  
     **Card Rarity**: B  
     It is covered with body hair that looks like bark at glance.

179. **Frog Hopper** | Hopper/???  
     **Card Rarity**: A  
     It likes to play battle games with kids and be mischievous.

## Hare

180. **Fairy Hare** | Hare/Pixie  
     **Card Rarity**: E  
     It worries about what people think, and is very shy.

181. **Rocky Fur** | Hare/Golem  
     **Card Rarity**: D  
     Its body is hard as rocks, but too heavy to move quickly.

182. **Scaled Hare** | Hare/Zuum  
     **Card Rarity**: E  
     Its ability to jump is better than that of other Hares.

183. **Blue Hare** | Hare/Tiger  
     **Card Rarity**: E  
     Its blue body is considered to be cute and is high in popularity.

184. **Hare** | Hare/Hare  
     **Card Rarity**: E  
     Hare is a little monster, but it can move quickly and it is brave.

185. **Prince Hare** | Hare/Gali  
     **Card Rarity**: D  
     It contains deity blood and has a refined appearance.

186. **Four Eyed** | Hare/Suezo  
     **Card Rarity**: E  
     It accurately aims at its target with its four eyes.

187. **Jelly Hare** | Hare/Jell  
     **Card Rarity**: E  
     Its body is firmly constructed, except its flabby belly.

188. **Leaf Hare** | Hare/Plant  
     **Card Rarity**: E  
     When it excites, the leaf pattern on its back will become embossed.

189. **Evil Hare** | Hare/Monol  
     **Card Rarity**: E  
     Unlike other Hare monsters, it's a nocturnal monster.

190. **Wild Hare** | Hare/Worm  
     **Card Rarity**: D  
     This is a manly looking monster with famous bushy eyebrows.

191. **Purple Hare** | Hare/Naga  
     **Card Rarity**: E  
     It can launch sharp attacks, but it is weak in defense.

192. **Kung Fu Hare** | Hare/???  
     **Card Rarity**: B  
     It trains daily to master the ancient art of kung fu.

## Baku

193. **Magmax** | Baku/Dragon  
     **Card Rarity**: C  
     It is active, but it becomes lazy when it is kept by humans.

194. **Higante** | Baku/Golem  
     **Card Rarity**: C  
     It has an abundant knowledge, but it is not good at applying it.

195. **War Baku** | Baku/Durahan  
     **Card Rarity**: B  
     It functioned well as a shield in the ancient battle.

196. **Icebergy** | Baku/Tiger  
     **Card Rarity**: D  
     It is a glutton, so ordinary people do not raise it.

197. **Gontar** | Baku/Hare  
     **Card Rarity**: D  
     It is curious and wants to accept all kinds of challenges.

198. **Baku** | Baku/Baku  
     **Card Rarity**: D  
     It doesn't like trouble, and it is an innocent monster.

199. **Nussie** | Baku/Kato  
     **Card Rarity**: D  
     It is the type of monster that won't risk anything.

200. **Baku Clown** | Baku/Joker  
     **Card Rarity**: B  
     It was born as a tool of devils, but it forgets its evil duty.

201. **Giga Pint** | Baku/Jell  
     **Card Rarity**: D  
     It is quiet and patient. Its body of gel is very flexible.

202. **Shishi** | Baku/???  
     **Card Rarity**: A  
     It is said that anyone bitten by it will be lucky for a year.

## Gali

203. **Pink Mask** | Gali/Pixie  
     **Card Rarity**: D  
     This monster has a pattern of the Pixies' dance on its cloak.

204. **Stone Mask** | Gali/Golem  
     **Card Rarity**: D  
     It is a guardian deity for the Golem species.

205. **Scaled Mask** | Gali/Zuum  
     **Card Rarity**: D  
     Its mask is covered with scales like Dino and Zuum.

206. **Fanged Mask** | Gali/Tiger  
     **Card Rarity**: D  
     It is a descendant from Tiger. Its mask is framed by fangs.

207. **Furred Mask** | Gali/Hare  
     **Card Rarity**: D  
     It is a guardian deity for the Hare species.

208. **Gali** | Gali/Gali  
     **Card Rarity**: D  
     It is said that its existence is close to that of God.

209. **Suezo Mask** | Gali/Suezo  
     **Card Rarity**: D  
     Its single eye does not actually see anything.

210. **Aqua Mask** | Gali/Jell  
     **Card Rarity**: D  
     There is no gelled portion on the body of this monster.

211. **Colorful** | Gali/Plant  
     **Card Rarity**: D  
     It has a strong life force and loves peace.

212. **Galirous** | Gali/Monol  
     **Card Rarity**: D  
     Its cloak is as hard as platinum and is as smooth as silk.

213. **Brown Mask** | Gali/Worm  
     **Card Rarity**: D  
     There is an indescribable smile on its mask.

214. **Purple Mask** | Gali/Naga  
     **Card Rarity**: D  
     It is a guardian deity for the Naga species and likes battle.

215. **Happy Mask** | Gali/???  
     **Card Rarity**: S  
     Its bright expression make you forget about your worries.

## Kato

216. **Draco Kato** | Kato/Dragon  
     **Card Rarity**: C  
     It contains dragon's violent blood and likes battle.

217. **Blue Kato** | Kato/Tiger  
     **Card Rarity**: E  
     Its quick movement makes its enemy confused.

218. **Gordish** | Kato/Gali  
     **Card Rarity**: D  
     This monster seems to take a philosophical view of life.

219. **Kato** | Kato/Kato  
     **Card Rarity**: E  
     It looks like an old cat, and loves to drink olive oil.

220. **Citronie** | Kato/Suezo  
     **Card Rarity**: E  
     It likes using a fighting style that confuses its enemies.

221. **Pink Kato** | Kato/Mocchi  
     **Card Rarity**: E  
     This monster likes its own cherry blossom colored body.

222. **Tainted Cat** | Kato/Joker  
     **Card Rarity**: B  
     It rejects everything in this world and lives alone.

223. **Ninja Kato** | Kato/???  
     **Card Rarity**: A  
     It originated from a Kato with master Ninja skills.

## Zilla

224. **Pink Zilla** | Zilla/Pixie  
     **Card Rarity**: B  
     It has a beautiful white body, and strong confidence.

225. **Gooji** | Zilla/Tiger  
     **Card Rarity**: B  
     With its huge horn, it can even sink a large ship.

226. **Zilla** | Zilla/Zilla  
     **Card Rarity**: B  
     Few trainers keep it because it is a glutton and is too big.

227. **Gigalon** | Zilla/Jell  
     **Card Rarity**: B  
     It was believed the ocean had eyes until it was discovered.

228. **Deluxe Liner** | Zilla/???  
     **Card Rarity**: A  
     It swims confidently but there is something anxious about it.

## Bajarl

229. **Bajarl** | Bajarl/Bajarl  
     **Card Rarity**: C  
     It is naughty. When someone scolds it, it hides in a pot.

230. **Jaba** | Bajarl/Joker  
     **Card Rarity**: B  
     It is said that this monster was made to be used as an assassin.

231. **Boxer Bajarl** | Bajarl/???  
     **Card Rarity**: A  
     It has great footwork even without foot.

232. **Magic Bajarl** | Bajarl/???  
     **Card Rarity**: B  
     It is considered to be a descendant from Magic in FIMBA area.

233. **Ultrarl** | Bajarl/???  
     **Card Rarity**: A  
     It is said that this kind monster is a kind of hero of justice.

## Mew

234. **Mum Mew** | Mew/Pixie  
     **Card Rarity**: D  
     It is a Pixie monster that uses the body form of a Mew.

235. **Bowwow** | Mew/Tiger  
     **Card Rarity**: E  
     This monster is related to the Tiger family.

236. **Eared Mew** | Mew/Hare  
     **Card Rarity**: E  
     It doesn't want people to pick it up by its long ears.

237. **Mew** | Mew/Mew  
     **Card Rarity**: E  
     This cat doll monster loves to sing very much.

238. **Aqua Mew** | Mew/Jell  
     **Card Rarity**: E  
     A Jell, that had longed to be a Mew doll, has turned into this monster.

239. **Swimmer** | Mew/???  
     **Card Rarity**: B  
     Training in the water will make the entire body toned up.

## Phoenix

240. **Phoenix** | Phoenix/Phoenix  
     **Card Rarity**: A  
     This is a legendary monster that never gives up.

241. **Cinder Bird** | Phoenix/???  
     **Card Rarity**: S  
     Contrary to its looks, it is burning furiously inside.

## Ghost

242. **Ghost** | Ghost/Ghost  
     **Card Rarity**: C  
     A dead monster, that was loved by its trainer, has turned into this one.

243. **Chef** | Ghost/???  
     **Card Rarity**: A  
     It flies around cooking a large variety of food at one time.

## Metalner

244. **Love Seeker** | Metalner/Pixie  
     **Card Rarity**: A  
     It is a Metalner combined with a Pixie to understand "love."

245. **Metalner** | Metalner/Metalner  
     **Card Rarity**: B  
     It is considered to have come from a planet far from here.

246. **Metazorl** | Metalner/Suezo  
     **Card Rarity**: B  
     It is combined with an expressive monster: Suezo

247. **Chinois** | Metalner/???  
     **Card Rarity**: A  
     The dish that it cooked tastes so good that makes you smile.

## Suezo

248. **Pink Eye** | Suezo/Pixie  
     **Card Rarity**: D  
     It will become depressed if it is not treated as humans.

249. **Rocky Suezo** | Suezo/Golem  
     **Card Rarity**: D  
     It does not like sandy areas very much because it is quite heavy.

250. **Melon Suezo** | Suezo/Zuum  
     **Card Rarity**: E  
     It loves fruits and sometimes encroaches on someone's farm.

251. **Horn** | Suezo/Tiger  
     **Card Rarity**: E  
     In the IMa area, the horn of this monster is very very small.

252. **Furred Suezo** | Suezo/Hare  
     **Card Rarity**: E  
     This monster likes adventure and is always exploring.

253. **Orion** | Suezo/Gali  
     **Card Rarity**: D  
     It is said that a microcosm can be seen deep inside its eye.

254. **Suezo** | Suezo/Suezo  
     **Card Rarity**: E  
     It is popular because of its human-like expressions

255. **Clear Suezo** | Suezo/Jell  
     **Card Rarity**: E  
     It is a Jell monster that has transformed itself to Suezo.

256. **Green Suezo** | Suezo/Plant  
     **Card Rarity**: E  
     Unlike other Suezo monsters, it's an earnest monster.

257. **Red Eye** | Suezo/Monol  
     **Card Rarity**: E  
     Its red eye is due to the fact that it stays up late.

258. **Fly Eye** | Suezo/Worm  
     **Card Rarity**: D  
     Everything it looks at is seen in a multiplied form.

259. **Purple Suezo** | Suezo/Naga  
     **Card Rarity**: E  
     It thinks that to put forth its maximum strength is embarrassing.

260. **Gold Suezo** | Suezo/???  
     **Card Rarity**: S  
     This is a very rare monster, for its body is the color gold.

261. **Silver Suezo** | Suezo/???  
     **Card Rarity**: A  
     This is also rare, for its body is the color silver.

262. **Bronze Suezo** | Suezo/???  
     **Card Rarity**: B  
     It is another rare one, for its body is the color bronze.

263. **Birdie** | Suezo/???  
     **Card Rarity**: B  
     Say, "Nice Shot" when you want to praise it for its good work.

264. **Sueki Suezo** | Suezo/???  
     **Card Rarity**: C  
     It is a man made monster, and its form is based on Suezo.

## Jill

265. **Pong Pong** | Jill/Pixie  
     **Card Rarity**: C  
     This one is a meat lover and a glutton.

266. **Pierry** | Jill/Tiger  
     **Card Rarity**: C  
     This monster is used to gather lumps of ice in high mountains.

267. **Wondar** | Jill/Hare  
     **Card Rarity**: C  
     It always forgets what it is supposed to do.

268. **Bengal** | Jill/Kato  
     **Card Rarity**: C  
     All Jills are gluttons. This one loves to eat fruits.

269. **Zorjil** | Jill/Suezo  
     **Card Rarity**: C  
     It has a high IQ and it often gets lost in its thoughts.

270. **Jill** | Jill/Jill  
     **Card Rarity**: C  
     This legendary monster can be found in the snow mountains.

271. **Skull Capped** | Jill/Joker  
     **Card Rarity**: B  
     It is scheming but not enough smart to deceive someone.

272. **Pithecan** | Jill/???  
     **Card Rarity**: A  
     There is a scholar who says that humans are descended from this monster.

## Mocchi

273. **Manna** | Mocchi/Pixie  
     **Card Rarity**: E  
     It believes that it is the most beautiful monster of all.

274. **Draco Mocchi** | Mocchi/Dragon  
     **Card Rarity**: C  
     Contrary to its cute movements, it is a violent monster.

275. **KnightMocchi** | Mocchi/Durahan  
     **Card Rarity**: B  
     Some sections of its body have been hardened like armor.

276. **Fake Penguin** | Mocchi/Tiger  
     **Card Rarity**: E  
     It is similar to a penguin, but they are not related at all.

277. **Nyankoro** | Mocchi/Kato  
     **Card Rarity**: E  
     Since it is a descendant from Kato, it looks like and old cat.

278. **Mocchi** | Mocchi/Mocchi  
     **Card Rarity**: E  
     Its sticky skin and cute movements attaract a lot of trainers.

279. **Hell Pierrot** | Mocchi/Joker  
     **Card Rarity**: B  
     Though it is a descendant from Joker, it cannot turn into a complete fiend.

280. **Gelatine** | Mocchi/Jell  
     **Card Rarity**: E  
     Its flabby looking body is actually firmer than it appears.

281. **Gentle Mocchi** | Mocchi/???  
     **Card Rarity**: S  
     It is a phantom monster and only a skillful and lucky trainer can get it.

282. **Caloriena** | Mocchi/???  
     **Card Rarity**: A  
     This monster never ceases its exercise and diet regiment.

283. **Mocchini** | Mocchi/???  
     **Card Rarity**: B  
     It always wears black tights in order to protect its legs. It is a fast runner

## Joker

284. **Hell Heart** | Joker/Pixie  
     **Card Rarity**: B  
     According to legends, it was an atrocious monster.

285. **Flare Death** | Joker/Dragon  
     **Card Rarity**: B  
     A descendant of a dragon has turned into an atrocious Joker.

286. **Tombstone** | Joker/Golem  
     **Card Rarity**: B  
     This is a Joker monster with the strength of the Golem species.

287. **Blue Terror** | Joker/Tiger  
     **Card Rarity**: B  
     It painlessly kills its enemy with one deadly attack.

288. **Odium** | Joker/Bajarl  
     **Card Rarity**: A  
     It is said that its spell causes its enemy to be unable to move.

289. **Joker** | Joker/Joker  
     **Card Rarity**: B  
     This monster was so atrocious that it was once sealed up.

290. **Bloodshed** | Joker/???  
     **Card Rarity**: A  
     This is the most aggressive monster of all the Joker species.

## Gaboo

291. **Frozen Gaboo** | Gaboo/Tiger  
     **Card Rarity**: E  
     Its body temperature is always kept low even during summer.

292. **Dokoo** | Gaboo/Joker  
     **Card Rarity**: B  
     It carries a type of poison that paralyzes other monsters.

293. **Gaboo** | Gaboo/Gaboo  
     **Card Rarity**: E  
     Contrary to its scary appearance, it is earnest and shy.

294. **Jelly Gaboo** | Gaboo/Jell  
     **Card Rarity**: E  
     It is similar to Jell, but it has no core like that of Jell.

295. **GabooSoilder** | Gaboo/???  
     **Card Rarity**: B  
     This monster is hot blooded, but often make careless mistakes.

## Jell

296. **Pink Jam** | Jell/Pixie  
     **Card Rarity**: D  
     This monster smells slightly sweet. But it is inedible.

297. **Wall Mimic** | Jell/Golem  
     **Card Rarity**: D  
     It likes to turn into a stone wall in order to tease people.

298. **Scaled Jell** | Jell/Zuum  
     **Card Rarity**: E  
     Its gelled sections are harder than its scaled sections.

299. **Icy Jell** | Jell/Tiger  
     **Card Rarity**: E  
     There is a type of confectionery that is modeled after this one.

300. **Muddy Jell** | Jell/Hare  
     **Card Rarity**: E  
     This monster is not a descendant from Gaboo.

301. **Noble Jell** | Jell/Gali  
     **Card Rarity**: D  
     This monster has high pride, but it is also very earnest.

302. **Eye Jell** | Jell/Suezo  
     **Card Rarity**: E  
     This monster can see through everything with its eye ball.

303. **Jell** | Jell/Jell  
     **Card Rarity**: E  
     It transfigures its gelled body and attacks its enemies.

304. **Chloro Jell** | Jell/Plant  
     **Card Rarity**: E  
     It needs more water than other Jell monsters.

305. **Clay** | Jell/Monol  
     **Card Rarity**: E  
     Though its name is Clay, it is rather similar to coal tar.

306. **Worm Jell** | Jell/Worm  
     **Card Rarity**: D  
     Its eyes are located in the lower portion of its body.

307. **Purple Jell** | Jell/Naga  
     **Card Rarity**: E  
     Its memory is so bad that it needs a lot of looking after.

308. **Metal Jell** | Jell/???  
     **Card Rarity**: B  
     It is very rare. Even if you find it, it will run away quickly.

## Undine

39. **Siren** | Undine/Joker  
    **Card Rarity**: B  
    It has the ability to not only manipulate water but also wind.

40. **Undine** | Undine/Undine  
    **Card Rarity**: B  
    This is a legendary monster living deep in the ocean.

41. **Mermaid** | Undine/???  
    **Card Rarity**: A  
    This monster appears in fairy tales once in a while.

## Niton

312. **Ammon** | Niton/Golem  
     **Card Rarity**: D  
     It usually stays completely still in one place like a fossil.

313. **Knight Niton** | Niton/Durahan  
     **Card Rarity**: B  
     Its shell is the hardest of all the Niton species.

314. **Stripe Shell** | Niton/Kato  
     **Card Rarity**: D  
     This monster is very patient when fighting.

315. **Alabia Niton** | Niton/Bajarl  
     **Card Rarity**: B  
     "Alabia" means to appear and disappear.

316. **Metal Shell** | Niton/Metalner  
     **Card Rarity**: B  
     Its body is made out of a strange substance.

317. **Clear Shell** | Niton/Jell  
     **Card Rarity**: D  
     Its gelled section is almost as hard as its shell.

318. **Niton** | Niton/Niton  
     **Card Rarity**: D  
     When something surprises it, it hides in its shell quickly.

319. **Baum Kuchen** | Niton/Mock  
     **Card Rarity**: B  
     "Baumkuchen" is German for "tree cake." This is similar to it.

320. **Dribbler** | Niton/???  
     **Card Rarity**: A  
     "Kickball" is the most popular sport in the IMa area.

321. **Radial Niton** | Niton/???  
     **Card Rarity**: A  
     It is said that there was a play titled "Cars" in the ancient era.

322. **Disc Niton** | Niton/???  
     **Card Rarity**: B  
     It has a pattern design that looks like a disc stone.

## Mock

323. **Ebony** | Mock/Joker  
     **Card Rarity**: A  
     This monster lives to confuse and surprise humans.

324. **Mock** | Mock/Mock  
     **Card Rarity**: B  
     This tree-like monster has a crooked character.

325. **White Birch** | Mock/???  
     **Card Rarity**: A  
     It is more resistant to cold than other Mock monsters.

326. **Pole Mock** | Mock/???  
     **Card Rarity**: S  
     It stands in dark places and eats bugs that were lured.

## Ducken

327. **Blocken** | Ducken/Golem  
     **Card Rarity**: C  
     This monster's duck-like shape is made up of several rocks.

328. **Ticken** | Ducken/Suezo  
     **Card Rarity**: C  
     It is said that it contains the secret of the lost technology.

329. **Ducken** | Ducken/Ducken  
     **Card Rarity**: C  
     This toy-like monster is an artifact of the ancient era.

330. **Watermelony** | Ducken/???  
     **Card Rarity**: A  
     On the beach, it is often mistaken for a watermelon.

331. **Cawken** | Ducken/???  
     **Card Rarity**: A  
     Some people consider this monster to be unlucky.

## Plant

332. **Queen Plant** | Plant/Pixie  
     **Card Rarity**: D  
     This monster is considered to be the strongest in the FIMBA area.

333. **Rock Plant** | Plant/Golem  
     **Card Rarity**: D  
     It brings forth buds out of hard rock. Its life force is strong.

334. **Scaled Plant** | Plant/Zuum  
     **Card Rarity**: E  
     Its green body is luminous at night, so many people have it.

335. **Blue Plant** | Plant/Tiger  
     **Card Rarity**: E  
     This monster is obedient, easy to take care of, and colorful.

336. **Hare Plant** | Plant/Hare  
     **Card Rarity**: E  
     It is so active that it hardly ever remains in the same place.

337. **Gold Plant** | Plant/Gali  
     **Card Rarity**: D  
     Noble people tend to like this flashy Plant monster.

338. **Usaba** | Plant/Suezo  
     **Card Rarity**: E  
     It is more difficult to grow than any other Plant monster.

339. **Mirage Plant** | Plant/Jell  
     **Card Rarity**: E  
     Its quiet character is often mentioned in sad songs.

340. **Plant** | Plant/Plant  
     **Card Rarity**: E  
     Thought it's not powerful, it can win by attacking repetitively.

341. **Black Plant** | Plant/Monol  
     **Card Rarity**: E  
     A soot-like substance makes its body black.

342. **Fly Plant** | Plant/Worm  
     **Card Rarity**: D  
     It hates its name and is gets mad when someone calls its name.

343. **Weeds** | Plant/Naga  
     **Card Rarity**: E  
     It can survive for several days without being fed.

344. **Reggae Plant** | Plant/???  
     **Card Rarity**: B  
     This monster loves a certain kind of music very much.

## Monol

345. **Romper Wall** | Monol/Pixie  
     **Card Rarity**: D  
     It's so friendly that it often tries to take too much care of humans.

346. **Obelisk** | Monol/Golem  
     **Card Rarity**: D  
     It is rather active, so it cannot be used as a stone wall.

347. **Jura Wall** | Monol/Zuum  
     **Card Rarity**: E  
     Noodles, which are made by using its moist body, are tasty.

348. **Blue Sponge** | Monol/Tiger  
     **Card Rarity**: E  
     You are guaranteed a good night's sleep if you sleep on it.

349. **Furred Wall** | Monol/Hare  
     **Card Rarity**: E  
     Though it has fluffy hair, its body is hard.

350. **Ivory Wall** | Monol/Gali  
     **Card Rarity**: D  
     It has the pattern of God that is embossed on its body.

351. **Sandy** | Monol/Suezo  
     **Card Rarity**: E  
     Its conspicuous figure is easy to spot even if it has escaped.

352. **Ice Candy** | Monol/Jell  
     **Card Rarity**: E  
     During summer, its body is cool and comfortable to lie on.

353. **New Leaf** | Monol/Plant  
     **Card Rarity**: E  
     It'll be filled with energy when half of its body is buried.

354. **Monol** | Monol/Monol  
     **Card Rarity**: E  
     It will separate into pieces to cushion against enemy's attacks.

355. **Soboros** | Monol/Worm  
     **Card Rarity**: D  
     It has eyes unlike other Monol and can see its enemy.

356. **Asphaltum** | Monol/Naga  
     **Card Rarity**: E  
     Ants try to make colonies in it, but it is impossible.

357. **Galaxy** | Monol/???  
     **Card Rarity**: A  
     This monster relates to the Sky monster in the FIMBA area.

358. **Dominos** | Monol/???  
     **Card Rarity**: A  
     These monsters like to gather and form a line.

359. **Scribble** | Monol/???  
     **Card Rarity**: B  
     This monster is rarely found in the IMa area.

## Ape

360. **Rock Ape** | Ape/Golem  
     **Card Rarity**: D  
     It is a lazy monster and sits as still as a stone statue.

361. **Gibberer** | Ape/Hare  
     **Card Rarity**: E  
     Female trainers try not to teach it some kind of techniques.

362. **Bossy** | Ape/Gali  
     **Card Rarity**: D  
     Though it is one of the laziest monsters, it is quite capable.

363. **Tropical Ape** | Ape/Plant  
     **Card Rarity**: E  
     This monster sprays a scent that decreases fighting spirits.

364. **Ape** | Ape/Ape  
     **Card Rarity**: E  
     There are many legends regarding this one's laziness.

365. **Gold Dust** | Ape/???  
     **Card Rarity**: A  
     It seems to be decadent, but it is actually a hard worker.

## Worm

366. **Red Worm** | Worm/Pixie  
     **Card Rarity**: C  
     It may emerge as a beautiful monster once in a long while.

367. **Rock Worm** | Worm/Golem  
     **Card Rarity**: C  
     It has beautiful eyes, but they do not work well in the light.

368. **Scaled Worm** | Worm/Zuum  
     **Card Rarity**: D  
     It hides its legs and uses them only for quick movement.

369. **Drill Tusk** | Worm/Tiger  
     **Card Rarity**: D  
     Its drill-like tusks can break even hard rocks into pieces.

370. **Corone** | Worm/Hare  
     **Card Rarity**: D  
     A chocolate filled sweet pastry is named after this one.

371. **Mask Worm** | Worm/Gali  
     **Card Rarity**: C  
     Though it is a descendant from God, it is not all that tough.

372. **Eye Worm** | Worm/Suezo  
     **Card Rarity**: D  
     It has four fake eyes, and single real eye is usually hidden in its forehead.

373. **Jelly Worm** | Worm/Jell  
     **Card Rarity**: D  
     Its flabby body cushions against its enemy's attack.

374. **Flower Worm** | Worm/Plant  
     **Card Rarity**: D  
     The contrast of its red body and blue eyes is very beautiful.

375. **Black Worm** | Worm/Monol  
     **Card Rarity**: D  
     This monster is used to carry heavy objects.

376. **Worm** | Worm/Worm  
     **Card Rarity**: D  
     Its abilities are all in balance and it has no weak points.

377. **Purple Worm** | Worm/Naga  
     **Card Rarity**: D  
     It frightens its enemies with its multitude of shiny eyes.

378. **Express Worm** | Worm/???  
     **Card Rarity**: B  
     None can compete with its speed as long as it is on its line.

## Naga

379. **Ripper** | Naga/Pixie  
     **Card Rarity**: D  
     Its IQ is high, but it often mistakes itself for a human.

380. **Trident** | Naga/Golem  
     **Card Rarity**: D  
     It likes to kill its enemies with one deadly attack.

381. **Stinger** | Naga/Zuum  
     **Card Rarity**: E  
     It always holds its tongue out to cool its body temperature.

382. **Striker** | Naga/Tiger  
     **Card Rarity**: E  
     Its obedient character is rare for the Naga species.

383. **Edgehog** | Naga/Hare  
     **Card Rarity**: E  
     It is descended from the Hare species, but it is very violent.

384. **Bazula** | Naga/Gali  
     **Card Rarity**: D  
     It is very careful and is rarely caught off guard.

385. **Cyclops** | Naga/Suezo  
     **Card Rarity**: E  
     It is so clever that it often disagrees with its trainer.

386. **Aqua Cutter** | Naga/Jell  
     **Card Rarity**: E  
     It is a Jell monster that is shaped in the form of Naga.

387. **Jungler** | Naga/Plant  
     **Card Rarity**: E  
     It hides in the forest and hunts animals.

388. **Crimson Eyed** | Naga/Monol  
     **Card Rarity**: E  
     It likes to damage its enemies little by little.

389. **Earth Keeper** | Naga/Worm  
     **Card Rarity**: C  
     It won't forgive anyone, even humans, that destroy nature.

390. **Naga** | Naga/Naga  
     **Card Rarity**: E  
     This one's cruel fighting style horrifies other monsters.

391. **Time Noise** | Naga/???  
     **Card Rarity**: S  
     The secret goal of this one has is to obtain immortality.

## Enemy Monsters

392. **Magma Heart** | Dragon/???  
     **Card Rarity**: Enemy
     It will immediately attack whoever invades its territory.

393. **Sniper** | Centaur/???  
     **Card Rarity**: Enemy
     This monster is feared as the dominator of the Mandy Desert.

394. **Sand Golem** | Golem/???  
     **Card Rarity**: Enemy
     It will not forgive anyone who breaks its quiet living.

395. **Wild Saurian** | Zuum/???  
     **Card Rarity**: Enemy
     It is an active Zuum-type monster that has become wild.

396. **Silver Face** | Arrow Head/???  
     **Card Rarity**: Enemy
     It has escaped from its trainers because of hard training.

397. **Kamui** | Tiger/???  
     **Card Rarity**: Enemy
     It escaped being executed for killing humans.

398. **Bloody Eye** | Hopper/???  
     **Card Rarity**: Enemy
     Its unique red eyes have caused it to be put on display.

399. **Crescent** | Kato/???  
     **Card Rarity**: Enemy
     An evil spirit is dwelling in it because of a forbidden drink.

400. **Zilla King** | Zilla/???  
     **Card Rarity**: Enemy
     It was awakened from an ancient sleep by a major earthquake.

401. **Blue Phoenix** | Phoenix/???  
     **Card Rarity**: Enemy
     Unlike other Phoenix monsters, it is very violent.

402. **Bighand** | Jill/???  
     **Card Rarity**: Enemy
     This kind of monsters are considered to be extinct.

403. **Mad Gaboo** | Gaboo/???  
     **Card Rarity**: Enemy
     A tool to make it obey is buried in its forehead.

404. **Burning Wall** | Monol/???  
     **Card Rarity**: Enemy
     This one escaped from its trainer while its stable was on fire.

405. **King Ape** | Ape/???  
     **Card Rarity**: Enemy
     This monster is feared as the dominator of the Parepare Jungle.

406. **Punisher** | Naga/???  
     **Card Rarity**: Enemy
     It is said that it was actually raised by another monster.

407. **White Suezo** | Suezo/???  
     **Card Rarity**: Enemy
     It was raised very carefully because of its pure white body.

408. **White Mocchi** | Mocchi/???  
     **Card Rarity**: Enemy
     It has changed the color of its skin in order to be conspicuous.
