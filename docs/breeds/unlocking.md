# Unlocking Every Breed

Managing to unlock every breed for the first time is a lengthy but incredibly rewarding endeavour.

## Starter Breeds
Of the 38 breeds in the game, 16 are available to play from the beginning. These are:

<details><summary><b>Unlocked Main Breeds</b></summary>

- Ape
- Arrowhead
- ColorPandora
- Gaboo
- Hare
- Hopper
- Jell
- Kato
- Mocchi
- Monol
- Naga
- Pixie
- Plant
- Suezo
- Tiger
- Zuum

</details>

Additionally, the following breeds are unlocked as **sub-breed** only from the beginning of the game:

<details><summary><b>Unlocked Sub Breeds</b></summary>

- Baku
- Centaur
- Dragon
- Gali
- Golem
- Henger
- Worm

</details>

All other breeds are locked, and any attempt to shrine them will result in you being informed that you are not authorised.

## Unlocking the Others

The remaining breeds must be unlocked via various requirements, which is very often one or several of:

- A [breeder rank](/tourneys/breeder-rank.md) requirement
- Finding an item during an [expedition](/expedition/)
- A house or stable [upgrade](./upgrades.md)
- Going on [Errantry](/errantry/) and defeating a boss

### Bajarl
**Breeder Rank:** 8th

After having purchased 3 house upgrades and 2 stable upgrades, you will have the ability to unlock
the final house upgrade. If you have at least 22,000G when May 4th comes around, Binto will arrive and
offer to upgrade your house one final time. You will then receive a cutscene to acquire the Bajarl Pot,
a combination item which will unlock Bajarl.

### Baku
**Breeder Rank:** 4th

After having purchased the first house upgrade, you will have the ability to unlock the first stable upgrade.
If you have at least 17,000G when May 4th comes around, Binto will arrive and offer to upgrade your stable.
After this is complete, Baku and Golem are unlocked.

### Beaclon
**Breeder Rank:** -

Raise any monster with the main breed Worm, and keep it subject to the following conditions:

- Either E, D, or C Rank
- Between 4 and 5 years old
- Loyalty 80+
- Fed 30+ Cup Jellies

If the following conditions all hold true, ensure that the Worm has 0 fatigue and <= 7 stress on June 4th 
(this will require resting on June 3rd). The worm will then [cocoon](./cocooning.md) and emerge as a Beaclon.

### Centaur
**Breeder Rank:** 4th

Complete the [initial Kawrea expedition](/expedition/maps.md#kawrea-i). Afterwards, send a monster
which has attained at least B rank to the Mandy (Pow) Errantry, between the months of March and August. Assuming
the monster returns uninjured, it will come back holding a Spear. Once back on the ranch, the Centaur will then
attack your monster. This is a one-off occurrence and will take an additional week, resulting
in extra lifespan loss to your monster, so plan accordingly. If your monster is outclassed, you may keep the spear even when giving up and there are no additional penalties for doing so.

### Dragon
**Breeder Rank:** 6th

Upgrade your stable at least once (see [Baku](./unlocking.md#baku)). Have participated in the IMa-FIMBA meet at least once. Now get a monster to specifically B rank and wait until July 1st. You will be invited to the Dragon invitiational. Defeat the Dragon Lagirus to win the Dragon Tusk, the combination item for Dragon.

### Ducken
**Breeder Rank:** -

Feed Cup Jellies to any monster until you reach your 5th Diamond Mark and recvieve the Quack Doll. What you do with this doll (give to monster or sell) is inconsequential. Now, go to the 
[Torles expedition](/expedition/maps.md#torles) with Rovest, and acquire the Strong Glue. After returning, acquire another Quack Doll via Diamond Marks, and build it using the Strong Glue this time. This is now the combination item for Ducken.

### Durahan
**Breeder Rank:** 4th

Attend the [Parepare expedition](/expedition/maps.md#parepare) with Kavaro, and acquire the Old Sheath.
Upon your return, visit Auntie Verde at the item shop. Now raise a monster to specifically A rank, and wait until Feb 1st. You will be invitied to the Durahan invitational on Feb 4th. Defeat the Durahan Leo to obtain the Double-Edged, the combination item for Durahan.

### Gali
**Breeder Rank:** 1st

Have a D rank or higher monster on the ranch on July 1st. You will be invited to the IMa-FIMBA qualifier on July 4th. Win this battle to progress to the IMa-FIMBA meet itself on August 4th. Attending this tournament for the first time, regardless of the outcome, will unlock Gali, Henger, Mew and Worm.

### Ghost
**Breeder Rank:** -

Have any monster die, then hold a funeral for it and build a shrine. After 140 weeks have passed, if you have >=8,000G on March 3rd, you may upgrade the shrine. After a further 140 weeks, you will be prompted for another upgrade, this time with a minimum requirement of 13,000G. The following March 3rd, you will receive a cutscene which will give you the Stick, the combination item for Ghost.

### Golem
_See [Baku](./unlocking.md#baku)._

### Henger
_See [Gali](./unlocking.md#gali)._

### Jill
**Breeder Rank:** 6th

Attend the [Torles expedition](/expedition/maps.md#torles) with Rovest, and acquire the Big Footprint. Now,
send a monster at B rank or above to the Papas (Spd) errantry. You will receive a warning about "Bighand". Defeat the Bighand monster (a Jill/???) to receive the Big Boots, Jill's combination item. Whether you fight Bighand or a different errantry monster is random, and may take several resets.

### Joker
**Breeder Rank:** -

Attend the [2nd Kawrea expedition](/expedition/maps.md#kawrea-ii) with Talico, and acquire the Joker Mask.
This is Joker's combination item.

### Metalner
**Breeder Rank:** 8th

The year must be 1011 or later, and you must have a B or higher rank monster on the ranch. Throughout the autumn, there ia a random chance that you will be visited by aliens. Once you have been visited, you will need to sit through several cutscenes. After these complete you will receive the crystal. Take this to the shrine and it will disappear, allowing you to acquire Metalners from CDs.

### Mew
_See [Gali](./unlocking.md#gali)._

### Mock
**Breeder Rank:** 4th

Visit the shop periodically until Verde gives you some seeds. These are a replacement for a normal new stock item, so it is only worth checking once per several months. This also means that you must reach B rank before the last item (Manseitan) is unlocked, although this should always happen in any serious playthrough. After the seeds are planted, around 10 years and many cutscenes later the planted tree will have grown and died. Now, have any monster die in fashion. You will then find the Mock alive on your ranch, and Mock will be unlocked.

### Niton
**Breeder Rank:** 4th

Raise any monster with the main breed Hopper to at least B rank, and have it at your ranch during winter. Ensure that the Hopper has 0 fatigue and <= 30 stress at the beginning of any week (rest is mandatory). It will then dig up the Hot Springs. Niton is now unlocked. You will additionally receive the Undine Slate, the combination item for Undine.

### Phoenix

Attend the [1st Kawrea expedition](/expedition/maps.md#kawrea-i) with Talico, and acquire the Phoenix Feather from the volcano, the only searchable location. If you do not locate it, you can also find the feather during the [2nd Kawrea expedition](/expedition/maps.md#kawrea-ii).

### Undine
_See [Niton](./unlocking.md#niton)._

### Worm
_See [Gali](./unlocking.md#gali)._

### Wracky
**Breeder Rank:** 7th

The year must be 1006 or higher. Have a monster with 90+ fame on the ranch. You will receive a doll in the mail. You will be given some inconsequential choices regarding what to do with the doll. Afterwards, have any monster die on the ranch. After the death cutscene, Wracky will appear on the ranch, and will be unlocked.

### Zilla
**Breeder Rank:** 6th

Uncover the hot springs (_see [Niton](./unlocking.md#niton)_), and ugrade your stable twice. Now, send a monster
which is at least B rank to the Torble (Ski) Errantry. Defeat the "Zilla King" (Zilla/???) monster to receive the Zilla Beard, Zilla's combination item. Whether you fight Zilla King or a different errantry monster is random, and may take several resets.