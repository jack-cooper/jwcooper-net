# Trainer Rank

There are 10 Trainer Ranks in the game. You begin the game at Trainer Rank 1. Each Official and Major 4 [tournament](/tournaments/) you beat will increase your trainer rank accordingly.

| Rank        | Requirement                              |
|-------------|------------------------------------------|
| **2nd**     | Victory at the Official D Tournament     |
| **3rd**     | Victory at the Official C Tournament     |
| **4th**     | Victory at the Official B Tournament     |
| **5th**     | Victory at the Official A Tournament     |
| **6th**     | Victory at the Official S Tournament     |
| **7th**     | Victory at any Major 4 Tournament        |
| **8th**     | Victory at any two Major 4 Tournaments   |
| **9th**     | Victory at any three Major 4 Tournaments |
| **Master**  | Victory at all Major 4 Tournaments       |
