module.exports = {
  title: 'MR2 Guide',
  base: '/mr2/guide/',
  configureWebpack: {
    resolve: {
      alias: {
        '@images': '.vuepress/images'
      }
    }
  },
  description: 'Comprehensive Guide to MR2',
  dest: './dist/mr2/guide',
  themeConfig: {
    sidebar: [
      {
        title: 'Breed Information',
        collapsable: false,
        children: [
          'breeds/breed-list',
          'breeds/unlocking',
          'breeds/lifespan',
          'breeds/cocooning'
        ]
      },
      {
        title: 'Raising Mechanics',
        collapsable: false,
        children: [
          'raising/stat-gains',
          'raising/fatigue-stress',
          'raising/loyalty',
          'raising/form-nature',
          'raising/motivation'
        ]
      },
      {
        title: 'Errantry',
        collapsable: false,
        children: ['errantry/']
      },
      {
        title: 'Items',
        collapsable: false,
        children: ['items/']
      },
      {
        title: 'Techniques',
        collapsable: false,
        children: ['techs/', 'techs/tables']
      },
      {
        title: 'Tournaments',
        collapsable: false,
        children: [
          'tourneys/',
          'tourneys/breeder-rank',
          'tourneys/calendar',
          'tourneys/battle-specials'
        ]
      },
      {
        title: 'Combination',
        collapsable: false,
        children: ['combination/', 'combination/advanced']
      },
      {
        title: 'Expeditions',
        collapsable: false,
        children: ['expedition/', 'expedition/maps']
      },
      {
        title: 'The Shrine',
        collapsable: false,
        children: [
          'shrine/',
          'shrine/cd-list',
          'shrine/cd-generation',
          'shrine/slating'
        ]
      },
      {
        title: 'Miscellaneous',
        collapsable: false,
        children: [
          'misc/faq',
          'misc/common-misconceptions',
          'misc/birthday-songs',
          'misc/unsolved-mysteries',
          'misc/play',
          'misc/mr2av'
        ]
      }
    ]
  }
}
