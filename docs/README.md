---
home: true
heroImage: /hero.png
heroText: Project EN-Wiki
tagline: The MR2 Guide
actionText: Get Started →
actionLink: breeds/breed-list.md
features:
- title: This is very much a work in progress
  details: Every effort has been made to curate this information as best I can but there may be mistakes. Many sections are also missing content, these are being worked on.
- title: Looking for the combination calculator?
  details: "It's back up and being worked on. If you'd prefer the old version there's a pin in #mr2 from June 16th 2019. "
- title: Contributing
  details: Want to help? Reach out to me (Tsmuji#4177) on Discord
---