const express = require('express')
const fs = require('fs')
const history = require('connect-history-api-fallback')

const app = express()
const port = 3000

/* HTTPS to be handled by NGINX reverse proxy on the server */

app.get('/football', (_req, res) => {
  fs.readFile('/standings.json', (err, data) => {
    if (err) {
      res.send({ error: 'There was an error processing your request.' })
    } else {
      res.send(JSON.parse(data))
    }
  })
})

const buildDir = 'dist'
app
  .use(express.static(buildDir)) // First usage of the static file middleware will catch the majority of the static file requests
  .use(history()) // To allow vue-router to work alongside history API
  .use(express.static(buildDir)) // Included twice so that rewritten requests to static content can also be served
  .listen(port, () => console.log(`jwcooper.net is running on port ${port}`))
